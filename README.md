# README #

Deployer un cluster docker swarm pour les services suivants: 

- n kafka_broker #n depend du nombre de serveur en mode "worker" dans le cluster 
- 3 zookeeper
- 1 kafka-manager #disponible sur le port 9000

1: Créer un cluster avec docker swarm.

- docker swarm init
- docker swarm join token TOKEN 

2: sur la vm manager contenant docker-compose.yml

- docker network create –attachable -d overlay cluster_network #Nom du cluster cluster_netork
- docker stack deploy -c docker-compose.yml STACK #Nom du sytème (remplacer STACK)